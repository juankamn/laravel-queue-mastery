<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldBeUniqueUntilProcessing;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\Middleware\ThrottlesExceptions;
use Illuminate\Queue\Middleware\WithoutOverlapping;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;

// ShouldBeUnique only create one instance of this one
// ShouldBeUniqueUntilProcessing only create one instance of this one until processing
class Deploy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
//        Redis::throttle('deployments')
//            ->allow(10)
//            ->every(60)
//            ->block(10)
//            ->then(function (){
//                info('Start deploying...');
//                sleep(5);
//                info('Finish deploying');
//            });

        // Redis method to work with currency
//        Redis::funnel('deployments')
//            ->limit(5)
//            ->block(10)
//            ->then(function (){
//                info('Start deploying...');
//                sleep(5);
//                info('Finish deploying');
//            });

        // Cache method to work with currency
//        Cache::lock('deployments')->block(10, function () {
//            info('Start deploying...');
//            sleep(5);
//            info('Finish deploying');
//        });
        info('Start deploying...');
        sleep(5);
        info('Finish deploying');
    }

//    public function uniqueId()
//    {
//        return 'deployments';
//    }
//
//    public function uniqueFor()
//    {
//        return 60;
//    }

    public function middleware()
    {
        return [
//            new WithoutOverlapping('deployments', 10)
            new ThrottlesExceptions(10) // Util si se está esperando respuesta de una tercera parte
        ];
    }
}
