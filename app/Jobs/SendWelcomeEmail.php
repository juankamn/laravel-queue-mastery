<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendWelcomeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
//    public $timeout = 1;  // timeout for the job
//    public $tries = 3;  // tries for the job before throwing an exception

//    public $tries = -1;  // with retryUntil() method, retries infinit times in this period of time
//    public $backoff = 2; // time between each retry of the job


    public $tries = 10;
    public $maxExceptions = 2;
//    public $backoff = [2, 10, 20]; // if backoff more longer than tries, laravel get last value for all attempts
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        throw new \Exception('Failed');
//        sleep(1);

        return $this->release();
//        return $this->release(2); // release back with delay of 2 seconds

//        sleep(3);
//
//        info('Send Hello!');
    }

//    public function retryUntil()
//    {
//        return now()->addMinute();
//    }

    public function failed($e)
    {
        info('Failed!');
    }
}
