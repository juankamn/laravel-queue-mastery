<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeEncrypted;  // To encrypt the info in the payload
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldBeUniqueUntilProcessing;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class TestJob implements ShouldQueue, ShouldBeEncrypted, ShouldBeUniqueUntilProcessing
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $connection = 'redis';
    public $queue = 'notifications';
    public $backoff = 30; //or array or method
    public $timeout = 60;
    public $tries = 3;
    public $delay = 300;
    public $afterCommit = true;
    public $shouldBeEncrypted = true;

    public $uniqueId = 'products';
    public $uniqueFor = 10;

    public $deleteWhenMissingModels = true;  // delete the job from the queue without throwing an exception, don't save on failed jobs


    public $secret;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($secret)
    {
        $this->secret = $secret;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
    }

    public function uniqueId()
    {
        return $this->product->id;
    }

    public function retryUntil()
    {
        return now()->addDay();
    }
}
