<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
//    (new App\Jobs\SendWelcomeEmail())->handle();
//    App\Jobs\SendWelcomeEmail::dispatch()->delay(5);
//    foreach(range(1,50) as $i)
//    {
//        App\Jobs\SendWelcomeEmail::dispatch();
//    }

//    App\Jobs\ProcessPayment::dispatch()->onQueue('payments');

//    $chain = [
//        new \App\Jobs\PullRepo(),
//        new \App\Jobs\RunTest(),
//        new \App\Jobs\Deploy(),
//    ];
//
//    \Illuminate\Support\Facades\Bus::chain($chain)->dispatch();
//
//    $batch = [
//        new \App\Jobs\PullRepo('laracast/project1'),
//        new \App\Jobs\PullRepo('laracast/project2'),
//        new \App\Jobs\PullRepo('laracast/project3'),
//    ];

//    \Illuminate\Support\Facades\Bus::batch($batch)
//        ->allowFailures()
//        ->catch(function ($batch, $e){
//            //
//        })
//        ->then(function ($batch){
//            //
//        })
//        ->finally(function ($batch){
//            //
//        })
//        ->onQueue('deployments')
//        ->onConnection('database')
//        ->dispatch();


//    $batch = [
//        [
//            new \App\Jobs\PullRepo('laracast/project1'),
//            new \App\Jobs\RunTest('laracast/project1'),
//            new \App\Jobs\Deploy('laracast/project1'),
//        ],
//        [
//            new \App\Jobs\PullRepo('laracast/project2'),
//            new \App\Jobs\RunTest('laracast/project2'),
//            new \App\Jobs\Deploy('laracast/project2'),
//        ],
//        [
//            new \App\Jobs\PullRepo('laracast/project3'),
//            new \App\Jobs\RunTest('laracast/project3'),
//            new \App\Jobs\Deploy('laracast/project3'),
//        ]
//    ];
//
//    \Illuminate\Support\Facades\Bus::batch($batch)
//        ->allowFailures()
//        ->dispatch();

//
//    \Illuminate\Support\Facades\Bus::chain([
//        new \App\Jobs\Deploy(),
//        function () {
//            \Illuminate\Support\Facades\Bus::batch(
//                [
//                    [
//                        new \App\Jobs\PullRepo('laracast/project1'),
//                        new \App\Jobs\RunTest('laracast/project1'),
//                        new \App\Jobs\Deploy('laracast/project1'),
//                    ],
//                    [
//                        new \App\Jobs\PullRepo('laracast/project2'),
//                        new \App\Jobs\RunTest('laracast/project2'),
//                        new \App\Jobs\Deploy('laracast/project2'),
//                    ],
//                    [
//                        new \App\Jobs\PullRepo('laracast/project3'),
//                        new \App\Jobs\RunTest('laracast/project3'),
//                        new \App\Jobs\Deploy('laracast/project3'),
//                    ]
//                ]
//            )
//                ->allowFailures()
//                ->dispatch();
//        }
//    ])->dispatch();


//    App\Jobs\Deploy::dispatch();


//    \Illuminate\Support\Facades\DB::transaction(function (){
//       $user = \App\Models\User::create([
//           'name' => 'Juan',
//           'email' => 'juan@email.com',
//           'password' => password_hash('macias')
//       ]);
//
//       \App\Jobs\SendWelcomeEmail::dispatch($user)->afterCommit(); // after transaction commit $user
//    });

    \App\Jobs\TestJob::dispatch('THIS_IS_THE_SECRET');

    return view('welcome');
});
